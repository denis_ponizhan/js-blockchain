const 	path 		= require('path'),
		express 	= require('express'),
		exphbs 		= require('express-handlebars'),
		bodyParser 	= require('body-parser'),
		SHA256 		= require('crypto-js/sha256'),
		fs 			= require('fs'),
		app 		= express();

/*****************************
* Server logic
*****************************/

app.use(bodyParser());

app.engine('.hbs', exphbs({
    defaultLayout: 'main',
    extname: '.hbs',
    layoutsDir: path.join(__dirname, 'views/layouts')
}))

app.set('view engine', '.hbs')

app.set('views', path.join(__dirname, 'views'))

app.get('/', (request, response) => {
    response.render('home')
})

app.post('/endpoint', function(req, res){
	res.send(req.body);
	jsChain.addBlock(new Block(Date.now(), req.body));
});

app.post('/view', function(req, res){
	res.send(fileBuffer.readBlockchainList());
});

app.post('/validate', function(req, res){
	res.send({message: jsChain.checkValid()});
});

app.listen(3000);

/*****************************
* Blockchain logic
*****************************/

class Block {
	/*
	* Initialize Genesis or new block to add to blockchain
	*/
	constructor(timestamp, data, genesisIndex, genesisPrevHash) {
		this.index = genesisIndex || jsChain.getLastIndex();
		this.previousHash = genesisPrevHash || jsChain.getPrevHash();
		this.timestamp = timestamp;
		this.data = data;
		this.hash = this.calculateHash();
	}

	calculateHash() {
		return SHA256(this.index + this.previousHash + this.timestamp + this.data).toString();
	}
}

class Blockchain {
	constructor() {
		/*
		* Initialize constructor with existing blockchain on blockchain.json, or started with Genesis
		*/
		this.chain = fileBuffer.readBlockchainList() || [this.createGenesis()];
	}

	createGenesis() {
		/*
		* Hardcoded first blockchain block
		*/
		return new Block(1254862800, {"user":"MobiDev","value":"Rocks!"}, 1, "0");
	}

	getLastIndex() {
		return this.chain.length + 1;
	}

	latestBlock() {
		return this.chain[this.chain.length - 1];
	}

	addBlock(newBlock) {
		/*
		* If empty blockchain.json, get started with Genesis
		*/
		if (!fileBuffer.readBlockchainList()) {
			this.chain = [this.createGenesis()];
		}

		newBlock.previousHash = this.latestBlock().hash;
		newBlock.hash = newBlock.calculateHash();
		newBlock.index = this.getLastIndex();

		this.chain.push(newBlock);
		fileBuffer.addToBlockchainList();
	}

	getPrevHash() {
		return this.latestBlock().hash;
	}

	checkValid() {
		let message = "";
		let validated = fileBuffer.readBlockchainList();
		
		if(validated) {
			for(let i = 0; i < validated.length; i++) {
				const currentBlock = validated[i];
				console.log(currentBlock)
				/*
				* If find failed block, return it id to client
				*/
				if (currentBlock.hash !== new Block().calculateHash.call(currentBlock)) {
					message = "Failed block id = " + currentBlock.index;
					break;
				} else {
					message = "Blockchain is valid";
				}
			}
		} else {
			message = "Blockchain is empty";
		}
		return message;

	}
}

class FileSaver {
	/*
	* blockchain.json - file to save chain data
	*/
	constructor() {
		this.blockchainList = 'blockchain.json';
	}

	addToBlockchainList() {
		fs.writeFileSync(this.blockchainList, JSON.stringify(jsChain.chain, null, 1));
	}

	readBlockchainList() {
		let rawdata = fs.readFileSync(this.blockchainList);  
		if (rawdata.length == 0) {
			return null;
		}
		let blockchain = JSON.parse(rawdata);  
		return blockchain; 
	}
}

const fileBuffer = new FileSaver();
const jsChain = new Blockchain();

