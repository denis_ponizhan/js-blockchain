
To start testing app, you need

1. Check what you have installed node.js and npm http://prntscr.com/klc5p5

node -v
npm -v

2. Clone this git repository http://prntscr.com/klc35u

git clone https://bitbucket.org/den_c2w/js-blockchain.git

3. Go to js-blockchain directory http://prntscr.com/klc44c

cd js-blockchain

4. Install packages http://prntscr.com/klc4ey

npm i

5. Run the node.js server http://prntscr.com/klc4vi

node index.js 

6. Open your browser and follow to  http://localhost:3000  http://prntscr.com/klc5b4

Testing ;)

To testing blockchain validate function 

1. Open app and try to validate with existing data, you should got a message https://prnt.sc/klcam5
2. Change hash of any block https://prnt.sc/klcaz2 and save file
3. Open app and try to validate one more time, you should got a message with failed block https://prnt.sc/klcb67